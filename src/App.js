import './App.css';
import "flowbite";
import InputComponent from './Components/InputComponent';
import { useState } from 'react';

function App() {
  const [person, setPerson]= useState([
    {
      id:1, 
      email: "yimeng1234@gmail.com",
      username:"yimeng",
      age:29,
    }
  ]);
  return (
    <div className="App">
      <div className='flex flex-col flex-wrap'>
      <h1 className="text-3xl font-bold underline bg-green-300">
      Input your Information
    </h1>
    <InputComponent person={person} setPerson={setPerson} />
      </div>
    
    </div>
  );
}

export default App;
