import React, { useState } from 'react'
import TableComponent from './TableComponent';

export default function InputComponent 
({person, setPerson}) 
{
    //for create state
    const [newObj, setNewObj]= useState({});

    // for get all user data from input
    const handleUserInput=(e)=>{
        const{name, value}= e.target;
        setNewObj({...newObj, [name]: value});
    };
    console.log(newObj);
    
    // handle submit
    const handleSubmit=()=>{
      setPerson([...person, {...newObj, id:person.length+1}]);
    };
    console.log(person);
    
  return (
    <div className=''>
   <div className=' flex flex-col w-96 mt-11 mx-auto'>
    {/* email */}
   <label for="input-group-1" class="block mb-2 text-sm font-medium text-gray-900 ">Your Email</label>
<div class="relative mb-6">
  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
    <svg aria-hidden="true" class="w-5 h-5 text-gray-500 " fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
  </div>
  <input type="text" name='username' class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 " placeholder="@gmail.com" onChange={handleUserInput}/>
</div>


{/* user name */}
<label for="website-admin" class="block mb-2 text-sm font-medium text-gray-900 ">Username</label>
<div class="flex">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
    @
  </span>
  <input type="text" name='email' class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 " placeholder="username" onChange={handleUserInput} />
</div>


{/* user's age */}
<label for="website-admin" class="block mb-2 text-sm font-medium text-gray-900 ">Age</label>
<div class="flex">
  <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md ">
    !
  </span>
  <input type="text" name='age' class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  " placeholder="0" onChange={handleUserInput}/>
</div>
<div className='mt-6'>
<a onClick={handleSubmit} class=" rounded px-5 py-2.5 overflow-hidden group bg-green-500 relative hover:bg-gradient-to-r hover:from-green-500 hover:to-green-400 text-white hover:ring-2 hover:ring-offset-2 hover:ring-green-400 transition-all ease-out duration-300">
<span class="absolute right-0 w-8 h-32 -mt-12 transition-all duration-1000 transform translate-x-12 bg-white opacity-10 rotate-12 group-hover:-translate-x-40 ease"></span>
<span class="relative">Submit</span>
</a>
</div>
   </div>
   <TableComponent info={person}/>
    </div>
  )
}
