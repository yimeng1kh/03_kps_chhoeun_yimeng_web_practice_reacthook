import React from 'react'

export default function TableComponent({info}) {
  return (
    <div className='flex flex-row'>
    <div className="w-2/5 m-auto mt-5">
     <div class="relative overflow-x-auto rounded-xl">
       <table class="w-full text-sm text-left text-gray-700 ">
         <thead class="text-xs text-gray-900 uppercase bg-gray-200">
           <tr>
             <th scope="col" class="px-6 py-3">
               ID
             </th>
             <th scope="col" class="px-6 py-3">
               Email
             </th>
             <th scope="col" class="px-6 py-3">
               USER NAME
             </th>
             <th scope="col" class="px-6 py-3">
               AGE
             </th>
           </tr>
         </thead>
         <tbody>
           {info.map((item) => (
             <tr key={item.id} class="odd:bg-green-200 even:bg-red-300">
               <th
                 scope="row"
                 class="px-6 py-4 font-medium text-black whitespace-nowrap "
               >
                 {item.id}
               </th>
               <td class="px-6 py-4">{item.email}</td>
               <td class="px-6 py-4">{item.username}</td>
               <td class="px-6 py-4">{item.age}</td>
             </tr>
           ))}
         </tbody>
       </table>
     </div>
   </div>
  </div>
  )
}
